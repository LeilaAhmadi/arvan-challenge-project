/* eslint-env node */
require('@rushstack/eslint-patch/modern-module-resolution')

module.exports = {
  root: true,
  parser: 'vue-eslint-parser',
  parserOptions: {
    ecmaVersion: 'latest',
  },
  extends: ['eslint:recommended', 'plugin:vue/vue3-essential', '@vue/eslint-config-prettier'],
  rules: {
    'comma-dangle': ['error', 'always-multiline'],
    'vue/multi-word-component-names': 'off',
  },
}
