/** @type {import('tailwindcss').Config} */
// eslint-disable-next-line no-undef

module.exports = {
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        primary: '#1c7cd5',
        k_black: {
          DEFAULT: '#212121',
        },
        success: {
          DEFAULT: '#1ABC9C',
        },
        info: {
          DEFAULT: '#1DA1F2',
        },
        warning: {
          DEFAULT: '#FFD32A',
        },
        error: {
          DEFAULT: '#cb2e25',
        },
      },
      borderRadius: {
        '3xl': '20px',
      },
    },
    container: {
      center: true,
      padding: {
        DEFAULT: '1rem',
        sm: '1rem',
        lg: '2rem',
        xl: '3rem',
        '2xl': '4rem',
      },
    },
  },
  plugins: [],
}
