import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useAppStore = defineStore('app', () => {
  let showSidebar = ref(true)
  let userInfo = ref(null)

  const isLoggedIn = computed(() => {
    return !!userInfo.value
  })

  function initStore() {
    let data = window.localStorage.getItem('arvan-user-info')
    userInfo.value = JSON.parse(data)
  }

  initStore()

  return { showSidebar, userInfo, isLoggedIn, initStore }
})
