import blankLayout from '@/layouts/blank.vue'
import defaultLayout from '@/layouts/default.vue'

export const routes = {
  home: {
    path: '/',
    meta: { title: 'Home', layout: defaultLayout },
    component: () => import('@/views/Home.vue'),
  },
  login: {
    path: '/login',
    meta: { title: 'Login', layout: blankLayout },
    component: () => import('@/views/auth/Login.vue'),
  },
  register: {
    path: '/register',
    meta: { title: 'Register', layout: blankLayout },
    component: () => import('@/views/auth/Register.vue'),
  },
  articles: {
    path: '/articles',
    meta: { title: 'Articles', layout: defaultLayout },
    component: () => import('@/views/article/List.vue'),
  },
  newArticle: {
    path: '/articles/add',
    meta: { title: 'Add Article', layout: defaultLayout },
    component: () => import('@/views/article/AddEdit.vue'),
  },
  editArticle: {
    path: '/articles/edit/:slug',
    meta: { title: 'Edit Article', layout: defaultLayout },
    component: () => import('@/views/article/AddEdit.vue'),
  },
}

export default Object.entries(routes).map(([name, route]) => {
  route.name = name
  return route
})
