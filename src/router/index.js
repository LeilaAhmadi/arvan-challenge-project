import { createRouter, createWebHistory } from 'vue-router'

import routes from './routes'

import { useAppStore } from '@/stores/app'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) return savedPosition
    if (to.hash) return { selector: to.hash }
    return { x: 0, y: 0 }
  },
})

router.beforeEach((to, from) => {
  let mainTitle = 'Arvan Challenge'
  document.title = mainTitle + (to.meta.title ? ` | ${to.meta.title}` : '')

  const app = useAppStore()

  const isAuthPage = ['login', 'register'].includes(to.name)

  if (!app.isLoggedIn && !isAuthPage) return { name: 'login' }

  if (app.isLoggedIn && isAuthPage) return { name: 'home' }
})

export default router
