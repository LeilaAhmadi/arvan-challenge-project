import { ofetch } from 'ofetch'
import { toast } from 'vue3-toastify'
import 'vue3-toastify/dist/index.css'

export const useFetch = ofetch.create({
  baseURL: 'https://api.realworld.io/api/',
  headers: {
    'Content-Type': 'application/json',
  },
  async onRequest({ options }) {
    const data = window.localStorage.getItem('arvan-user-info')
    const token = JSON.parse(data)?.token
    options.headers.Authorization = token ? 'Token ' + token : ''
  },
  async onResponse({ response }) {
    let errors = []
    if (!response.ok) {
      if (response._data.errors)
        errors = Object.entries(response._data.errors).map(([key, item]) => {
          return `${key} ${item[0]}`
        })
      if (response._data.message) errors.push(response._data.message)
      errors.forEach((error) => {
        toast.error(error, {
          position: toast.POSITION.TOP_RIGHT,
          transition: toast.TRANSITIONS.SLIDE,
          hideProgressBar: true,
        })
      })
    }
  },
})
