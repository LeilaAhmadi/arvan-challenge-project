export default {
  //======== AUTH =============//
  login: 'users/login',
  register: 'users',

  //======== ARTICLE =============//
  articles: 'articles',
  article: 'articles/{slug}',
  tags: 'tags',
}
